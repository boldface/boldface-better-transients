<?php
/**
 * Plugin Name: Boldface Better Transients
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-better-transients/
 * Description: Add properties and methods to interact with WordPress transients.
 * Version: 0.1
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI:
 * Domain Path: /languages
 * Text Domain: boldface-better-transients
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Define main plugin file
define( 'BOLDFACEBETTERTRANSIENTS', __FILE__ );

//* Start bootstraping the plugin
require( dirname( BOLDFACEBETTERTRANSIENTS ) . '/includes/bootstrap.php' );
add_action( 'plugins_loaded', array( $bootstrap = new boldface_better_transients_bootstrap( BOLDFACEBETTERTRANSIENTS ), 'register' ) );

//* Register activation and deactivation hooks
register_activation_hook( BOLDFACEBETTERTRANSIENTS , array( $bootstrap, 'activation' ) );
register_deactivation_hook( BOLDFACEBETTERTRANSIENTS , array( $bootstrap, 'deactivation' ) );
