<?php

defined( 'ABSPATH' ) or die();

function bbt_transient( $name ) {
  return new \Boldface\BetterTransients\transient( $name );
}
