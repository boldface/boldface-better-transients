<?php

namespace Boldface;

/**
 * Includes the file in the plugin
 *
 * @package Boldface
 */
class file_registry {

  /**
   * Instance of the file investigator
   *
   * @var file_investigator
   *
   * @access private
   * @since 0.1
   */
  private $file_investigator;

  /**
   * Initiate the file investigator
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    include_once( __DIR__ . '/file-investigator.php' );
    $this->file_investigator = new file_investigator();
  }

  /**
   * Load the class
   *
   * @param string $class The path the the class to load
   *
   * @access public
   * @since 0.1
   */
  public function load( $class ) {

    $file = $this->file_investigator->class( $class )->get_filename();

    if( file_exists( $file ) ) {
      require( $file );
      return;
    }
    $this->file_investigator->die();
  }
}
