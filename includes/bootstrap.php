<?php
/**
 * Boldface Better Transients Plugin
 *
 * @package Boldface\BoldfaceBetterTransients
 */

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

 /**
  * Class for bootstrapping the plugin
  */
class boldface_better_transients_bootstrap {

  /**
   * @var string Plugin basename
   *
   * @access private
   * @since 0.1
   */
  private $file;

  /**
   * Constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = is_string( $file ) ? plugin_basename( $file ) : '';
  }

  /**
   * Clone magic method to prevent cloning of this class.
   *
   * @access public
   * @since 0.1
   */
  public function __clone() {
    wp_die( 'Cloning of this class is not allowed.' );
  }

  /**
   * Method needs to be fired on plugins_loaded
   * Checkes if minimum requirements are met and loads the plugin
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Prior to loading this options, run the filter
    \add_filter( 'pre_option_soft_transients_self_destruct', array( $this, 'maybe_autoloaded_option' ), 10, 2 );

    //* Check to see if we need to auto destruct
    //* The option is autoloaded and cached to reduce database queries
    if( 'true' === get_option( 'soft_transients_self_destruct' ) ) {
      add_action( 'admin_notices', array( $this, 'self_destruct_error' ) );
      add_action( 'admin_notices', array( $this, 'self_destruct' ), 15 );

      return;
    }

    //* Autoloader
    require_once( __DIR__ . '/autoload.php' );
    spl_autoload_register( array( new \Boldface\autoload(), 'load' ) );

    //* Add action to the current hook at the next priority
    \add_action(
      'plugins_loaded',
      array( new \Boldface\BetterTransients\plugin( $this->file ), 'register' ),
      11
    );

    //* Include functions file
    $functions = \trailingslashit( WP_PLUGIN_DIR ) . plugin_dir_path( $this->file ) . 'functions.php';
    if( file_exists( $functions ) ) {
      require_once( $functions );
    }
  }

  /**
   * Add action on activation to maybe_self_destruct
   *
   * @access public
   * @since 0.1
   */
  public function activation() {
    add_action( 'shutdown', array( $this, 'maybe_self_destruct' ) );
  }

  /**
   * Fired on plugin deactivation
   *
   * @access public
   * @since 0.1
   */
  public function deactivation() {
  }

  /**
   * Checkes if minimum requirements are met and adds autoloading option
   *
   * @access public
   * @since 0.1
   */
  public function maybe_self_destruct() {
    //* Check version.
    //* Add autoloaded option to database to see if we need to self-destruct
    if( ! $this->version_check() ) {
      add_option( 'soft_transients_self_destruct', 'true', '', 'yes' );
    }
  }

  /**
   * Self destruct by deactivating the plugin
   *
   * @access public
   * @since 0.1
   */
  public function self_destruct() {
    deactivate_plugins( SOFTTRANSIENTS );
  }

  /**
   * Error message to display on self destruct
   *
   * @access public
   * @since 0.1
   */
  public function self_destruct_error() {
    printf( '
      <div class="error"><p>%1$s</p></div><style>#message.updated{ display: none; }</style>',
      __( 'Soft Transients plugin cannot be activated. PHP version 5.3 or greater required.', 'soft-transients' ) );
  }

  /**
   * If the option is autoloaded, load it.
   *
   * @param $default mixed  The default value of the option
   * @param $option  string The option name
   *
   * @access public
   * @since 0.1
   *
   * @return mixed The autoloaded option
   */
  public function maybe_autoloaded_option( $default = false, $option = '' ) {
    //*
    $notoptions = wp_cache_get( 'notoptions', 'options' );
    if( isset( $notoptions[ $option ] ) ) {
      return $default;
    }

    $alloptions = wp_load_alloptions();

    if( isset( $alloptions[ $option ] ) ) {
      $value = $alloptions[ $option ];
    } else {
      $value = wp_cache_get( $option, 'options' );
    }

    if( ! $value ) {
      $value = null;
    }

    return maybe_unserialize( $value );
  }

  /**
   * Check the PHP version
   * The main plugin file and this one will work with PHP 5.2. Other files
   * require at least PHP 5.3.
   *
   * @access private
   * @since 0.1
   */
  private function version_check() {
    if( version_compare( PHP_VERSION, '5.3', '<') )
    {
      return false;
    }
    return true;
  }
}
