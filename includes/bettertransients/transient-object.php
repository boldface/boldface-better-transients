<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for interacting with the WordPress Transients API when using an object cache
 *
 * @package Boldface\SoftTransients
 */
class transient_object {

  /**
   * @var string Name of the transient
   *
   * @access protected
   * @since 0.1
   */
  protected $name;

  /**
   * @var string Key of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $key;

  /**
   * @var mixed Value of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $value;

  /**
   * @var int Expiration of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $expiry;

  /**
   * @var bool Whether this is a site transient or not
   *
   * @access protected
   * @since 0.1
   */
  protected $site;

  /**
   * Constructor
   *
   * @param string $name Name of the transient
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $name ) {
    $this->name = $name;
  }

  /**
   * Return whether there exists a transient with the given name
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient exists
   */
  public function has() {
    return isset( $this->value ) ? $this->value : $this->has_value();
  }

  /**
   * Return whether there exists a transient with the given name
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient exists
   */
  protected function has_value() {
    return false === $this->get_cache_value() ? false : true;
  }

  /**
   * Get the value of the transient stored in the cache
   *
   * @access public
   * @since 0.1
   *
   * @return mixed The value of the transient
   */
  public function get_direct() {
    return isset( $this->value ) ? $this->value : $this->get_cache_value();
  }

  /**
   * Return the transient
   *
   * @access protected
   * @since 0.1
   *
   * @return mixed The value of the transient
   */
  protected function get_cache_value() {
    return $this->value = $this->site() ?
      \wp_cache_get( $this->name, 'site-transient' ) :
      \wp_cache_get( $this->name, 'transient' );
  }

  /**
   * Return a year from now since WordPres never expires object cache transients
   *
   * @access public
   * @since 0.1
   *
   * @return int Transient expiry
   */
  public function expiry() {
    return time() + YEAR_IN_SECONDS;
  }

  /**
   * Return false since WordPress never expires object cache transients
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is expired
   */
  public function is_expired() {
    return false;
  }

  /**
   * Return whether the transient is a site transient
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is a site transient
   */
  public function site() {
    return isset( $this->site ) ? $this->site : $this->get_site();
  }

  /**
   * Get the site
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is a site transient
   */
  protected function get_site() {
    return false !== \wp_cache_get( $this->name, 'site-transient' ) ?
      true : false;
  }

  /**
   * Autoload property is always false because of the way WordPress does
   * object caching.
   *
   * @access public
   * @since 0.1
   *
   * @return bool False
   */
  public function autoload() {
    return false;
  }

  /**
  * Autoload property is always false because of the way WordPress does
  * object caching.
   *
   * @access protected
   * @since 0.1
   *
   * @return bool False
   */
  protected function get_autoload() {
    return false;
  }

  /**
   * Return whether the transient is locked
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is locked
   */
  public function lock() {
    return isset( $this->lock ) ? $this->lock : $this->get_lock();
  }

  /**
   * Get the lock
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is locked
   */
  protected function get_lock() {
    $lock = new transient( 'lock_' . md5( $this->name ) );
    return $lock->has();
  }
}
