<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Interface for transient cache
 *
 * @package Boldface\SoftTransients
 */
interface interface_transient_cache {

  public function has();

  public function get_direct();

  public function expiry();

  public function is_expired();

  public function site();

  public function autoload();

  public function lock();

}
