<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for interacting with the WordPress Transients API when using a database
 *
 * @package Boldface\SoftTransients
 */
class transient_database implements interface_transient_cache {

  /**
   * @var string Name of the transient
   *
   * @access protected
   * @since 0.1
   */
  protected $name;

  /**
   * @var string Key of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $key;

  /**
   * @var mixed Value of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $value;

  /**
   * @var int Expiration of the transient stored in the database
   *
   * @access protected
   * @since 0.1
   */
  protected $expiry;

  /**
   * @var bool Whether this is a site transient or not
   *
   * @access protected
   * @since 0.1
   */
  protected $site;

  /**
   * Constructor
   *
   * @param string $name Name of the transient
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $name ) {
    $this->name = $name;
  }

  /**
   * Return whether there exists a transient with the given name
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient exists
   */
  public function has() {
    return isset( $this->key ) ? $this->key : $this->has_key();
  }

  /**
   * Return The key stored in the database
   *
   * @access protected
   * @since 0.1
   *
   * @return string The key stored in the database
   */
  protected function key() {
    return isset( $this->key ) ? $this->key : $this->get_key();
  }

  /**
   * Return whether there exists a transient key with the given name
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient key exists
   */
  protected function has_key() {
    return false === $this->get_site_option() && false === $this->get_option() ?
      false : true;
  }

  /**
   * Queries the database for the transient key
   *
   * @access protected
   * @since 0.1
   *
   * @return string Database key
   */
  protected function get_key() {
    return false !== $this->get_site_option() ?
      '_site_transient_' . $this->name : '_transient_' . $this->name;
  }

  /**
   * Queries the database for the transient and returns the value
   *
   * @access public
   * @since 0.1
   *
   * @return mixed The value of the transient
   */
  public function get_direct() {
    return $this->site() ?
      $this->get_site_option() : $this->get_option();
  }

  /**
   * Get transient from the options table
   *
   * @access protected
   * @since 0.1
   *
   * @return mixed Transient
   */
  protected function get_option() {
    return \get_option( "_transient_{$this->name}" );
  }

  /**
   * Get transient timeout from the options table
   *
   * @access protected
   * @since 0.1
   *
   * @return int Transient
   */
  protected function get_option_timeout() {
    return \get_option( "_transient_timeout_{$this->name}" );
  }

  /**
   * Get site transient from the options table
   *
   * @access protected
   * @since 0.1
   *
   * @return int Site transient
   */
  protected function get_site_option() {
    return \get_option( "_site_transient_{$this->name}" );
  }

  /**
   * Get transient timeout from the options table
   *
   * @access protected
   * @since 0.1
   *
   * @return int Transient timeout
   */
  protected function get_site_option_timeout() {
    return \get_option( "_transient_timeout_{$this->name}" );
  }

  /**
   * Get transient expiry
   *
   * @access public
   * @since 0.1
   *
   * @return int Transient expiry
   */
  public function expiry() {
    return isset( $this->expiry ) ?
      $this->expiry : $this->expiry = $this->get_expiry();
  }

  public function has_expiration() {
    return $this->get_expiration() > 0 ? true : false;
  }

  public function get_expiration() {
    return $this->expiry();
  }

  /**
   * Return whether the transient is expired
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is expired
   */
  public function is_expired() {
    return $this->expiry() < time() ? true : false;
  }

  /**
   * Queries the database for the transient timeout
   *
   * @access protected
   * @since 0.1
   *
   * @return int UNIX timestamp of expiration
   */
  protected function get_expiry() {
    return $this->site() ?
      $this->get_site_option_timeout() : $this->get_option_timeout();
  }

  /**
   * Get the site
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is a site transient
   */
  public function site() {
    return isset( $this->site ) ? $this->site : $this->get_site();
  }

  /**
   * Get the site
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is a site transient
   */
  protected function get_site() {
    return $this->site = '_site' === substr( $this->get_key(), 0, 5 ) ?
      true : false;
  }

  /**
   * Get the autoload value
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is autoloaded
   */
  public function autoload() {
    return isset( $this->autoload ) ? $this->autoload : $this->get_autoload();
  }

  /**
   * Get the autoload value from the database
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is autoloaded
   */
  protected function get_autoload() {
    global $wpdb;
    $name = "_transient_{$this->name}";

    $autoload = $wpdb->get_col( "
      SELECT autoload
      FROM $wpdb->options
      WHERE option_name
      LIKE '%$name%'
      LIMIT 1
    " );

    if( is_array( $autoload) && count( $autoload) > 0 ) {
      return $this->autoload = 'yes' === $autoload[0] ? true : false;
    }
    return false;
  }

  /**
   * Return whether the transient is locked
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is locked
   */
  public function lock() {
    return isset( $this->lock ) ? $this->lock : $this->get_lock();
  }

  /**
   * Get the lock
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is locked
   */
  protected function get_lock() {
    $lock = new transient( 'lock_' . md5( $this->name ) );
    return $lock->has();
  }
}
