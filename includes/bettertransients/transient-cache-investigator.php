<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Determine the type of cache the WordPress Transients API uses
 *
 * @package Boldface\SoftTransients
 */
class transient_cache_investigator {

  /**
   * @var string Name of the transient passed to the constructor
   *
   * @access protected
   * @since 0.1
   */
  protected $name;

  /**
   * @var object The object cache in use
   *
   * @access protected
   * @since 0.1
   */
  protected $cache_object;

  /**
   * Constructor
   *
   * @param string $name Name of the transient
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $name ) {
    $this->name = $name;
  }

  /**
   * Return an instance of the object cache
   *
   * @access public
   * @since 0.1
   *
   * @return object Object cache
   */
  public function get_cache_object() {
    return $this->cache_object = \wp_using_ext_object_cache() ?
      new transient_object( $this->name ) :
      new transient_database( $this->name );
  }
}
