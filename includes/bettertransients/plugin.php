<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for loading the Boldface Better Transients plugin
 *
 * @package Boldface\Transient
 */
class plugin {

  /**
   * @var string Plugin basename
   *
   * @access private
   * @since 0.1
   */
  private $file;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file = '' ) {
    $this->file = is_string( $file ) ? plugin_basename( $file ) : '';
  }

  /**
   * Method needs to be fired before init.
   * Add several actions to load the plugin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on initiation to load plugin text domain
    \add_action( 'init', [ $this, 'load_textdomain' ] );

    //* Add hook, if the _transient is sent via POST
    if( isset( $_POST[ '_transient' ] ) ) {
      \add_action( 'init', [ new transient_server_response(), 'init' ], -1 );
    }
  }

  /**
   * Load the text domain
   *
   * @access public
   * @since 0.1
   */
  public function load_textdomain() {
    \load_plugin_textdomain( 'boldface-transient', false, dirname( \plugin_basename( __FILE__ ) ) . '/languages' );
  }
}
