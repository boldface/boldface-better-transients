<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for interacting with expried WordPress transients
 *
 * @package Boldface\SoftTransients
 */
class transient_server {

  /**
   * @var array $args Array or arguments
   *
   * @access protected
   * @since 0.1
   */
  protected $args;

  /**
   * Schedule background process
   *
   * @param array $args Array of arguments
   *
   * @access public
   * @since 0.1
   */
  public function schedule( array $args = [] ) {

    //* Prevent infinite loop
    if( 'GET' !== $_SERVER[ 'REQUEST_METHOD' ] ) {
      return;
    }

    if( ! isset( $args[ 'name' ] ) || ! isset( $args[ 'process' ] ) ) {
      return;
    }

    if( isset( $this->args ) ) {
      return;
    }

    $this->args = [
      'cookies'    => $_COOKIE,
      'timeout'    => 0.001,
      'blocking'   => false,
      'sslverify'  => false,
      'body'       => [
        '_wpnonce'   => \wp_create_nonce( 'transient-nonce' ),
        '_transient' => [
          'site'       => isset( $args[ 'site' ] ) ? $args[ 'site' ] : false,
          'name'       => $args[ 'name' ],
          'value'      => isset( $args[ 'value' ] ) ? $args[ 'value' ] : null,
          'expiration' => isset( $args[ 'expiration' ] ) ? $args[ 'expiration' ] : 0,
          'process'    => $args[ 'process' ],
        ],
      ],
    ];

    \add_action( 'shutdown', [ $this, 'spawn' ] );
  }

  /**
   * Spawn new server
   *
   * @access public
   * @since 0.1
   */
  public function spawn() {
    \wp_safe_remote_post( \esc_url( \home_url() ), $this->args );
  }
}
