<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for interacting with the WordPress Transients API
 *
 * There are only six functions in the WordPress Transients API, yet it may
 * be the most understood API in WordPress core. The functions available are:
 *
 * set_transient()
 * set_site_transient()
 * get_transient()
 * get_site_transient()
 * delete_transient()
 * delete_site_transient()
 *
 * This class acts as a wrapper for get(), set(), and delete()
 *
 * transient::set()
 * transient::get()
 * transient::delete()
 *
 * Since transients are all stored in the options table, the site aspect
 * is abstracted from the getting, setting, and deleting of transients,
 * and replaced with its own method. Further, the option of autoloading or
 * not autoloading is now available to all transients.
 *
 * transient::site()
 * transient::autoload()
 *
 * The class adds several methods to extend the API. Transients are locked by
 * default, which means that they cannot be modified or deleted except by an
 * administrator using the sudo method.
 *
 * transient::has()
 * transient::rename()
 * transient::lock()
 * transient::sudo()
 *
 * And a method to access the value directly using the options API instead
 * of the transients API.
 *
 * transient::get_direct()
 *
 * And adds methods to interact with the expiration
 *
 * transient::has_expiration()
 * transient::get_expiration()
 * transient::is_expired()
 *
 * Examples:
 *
 * To set a site transient named Foo with a value of Bar
 * that expires in an hour and is set in the background:
 *
 * $example = new \Boldface\Transient\transient( 'Foo' );
 * $example->site( true )->background( true )->expires( HOUR_IN_SECONDS )->set( 'Bar' );
 *
 * This example uses the bbt_transient() helper function, gets the transient
 * in the background, provides a callback function that will execute if the
 * transient doesn't exist.
 *
 * $example = bbt_transient( 'test' )
 *   ->background( true )
 *   ->get( function(){
 *     return [
 *       'value' => 1,
 *       'expires' => HOUR_IN_SECONDS,
 *     ];
 * });
 *
 * @package Boldface\BetterTransients
 */
class transient {

  /**
   * @var string Name of the transient passed to the constructor
   *
   * @access protected
   * @since 0.1
   */
  protected $name;

  /**
   * @var mixed Value of the transient
   *
   * @access protected
   * @since 0.1
   */
  protected $value;

  /**
   * @var int UNIX timestamp of transient expiration
   *
   * @access protected
   * @since 0.1
   */
  protected $expiration;

  /**
   * @var bool Whether to run the request as a background process
   *
   * @access protected
   * @since 0.1
   */
  protected $background;

  /**
   * @var bool Whether the transient is autoloaded
   *
   * @access protected
   * @since 0.1
   */
  protected $autoload;

  /**
   * @var bool Whether the transient is locked
   *
   * @access protected
   * @since 0.1
   */
  protected $lock;

  /**
   * @var bool Whether the transient actions should be performed as a superuser
   *
   * @access protected
   * @since 0.1
   */
  protected $sudo;

  /**
   * @var bool Whether the transient is a site transient or not
   *
   * @access protected
   * @since 0.1
   */
  protected $site;

  /**
   * @var object The specific transient cache object
   *
   * @access protected
   * @since 0.1
   */
  protected $cache;

  /**
   * Constructor
   *
   * @param string $name Name of the transient
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $name ) {

    if( ! is_string( $name ) ) {
      \wp_die( 'Transient name must be a string.');
    }

    $this->sanitize_name( $name );

    $this->cache = ( new transient_cache_investigator( $this->name ) )
      ->get_cache_object();
  }

  /**
   * Return whether there exists a transient with the given name
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient exists
   */
  public function has() {
    return $this->cache->has();
  }

  /**
   * Set transient
   *
   * @param mixed $value  The value of the transient to be set
   * @param int   $expiry When the transient expires ( optional )
   *
   * @access public
   * @since 0.1
   */
  public function set( $value, $expiry = null ) {
    if( isset( $expiry ) ) {
      $this->expires( $expiry );
    }

    $pre = \apply_filters( 'Boldface\BetterTransients\pre_set_transient', false, $this->name );
    $pre = \apply_filters( "Boldface\BetterTransients\pre_set_transient_{$this->name}", false, $pre );

    if( false !== $pre ) {
      return $this->value = $pre;
    }

    $this->value = $value;
    $this->has() && ! $this->can_edit() ? $this->_return() : $this->_set();

    $value = \apply_filters( "Boldface\BetterTransients\setted_transient_{$this->name}", $value, $pre );
    $value = \apply_filters( 'Boldface\BetterTransients\setted_transient', $value, $this->name );

    return $this->value = $value;
  }

  /**
   * Set transient in foreground or background
   *
   * @access protected
   * @since 0.1
   */
  protected function _set() {
    $this->is_lock() ? $this->set_lock() : $this->_return();
    $this->is_background() ? $this->set_background() : $this->set_foreground();
  }

  /**
   * Wrapper for set_site_transient()
   *
   * @access protected
   * @since 0.1
   */
  protected function set_site_transient() {
    \set_site_transient( $this->name, $this->value, $this->expiration );
  }

  /**
   * Wrapper for set_transient()
   *
   * @access protected
   * @since 0.1
   */
  protected function set_transient() {
    \set_transient( $this->name, $this->value, $this->expiration );
  }

  /**
   * Determine whether to set the site transient or transient
   *
   * @access protected
   * @since 0.1
   */
  protected function set_foreground() {
    $this->is_site() ? $this->set_site_transient() : $this->set_transient();
  }

  /**
   *
   *
   * @access protected
   * @since 0.1
   */
  protected function set_background() {
    $this->background_process( 'set' );
  }

  /**
   * Set the transient lock
   *
   * @access protected
   * @since 0.1
   */
  protected function set_lock() {
    $lock = new transient( 'lock_' . md5( $this->name ) );
    $lock->lock( false )
      ->site( $this->is_site() )
      ->background( $this->is_background() )
      ->expires( $this->expiration )
      ->set( $this->value );
  }

  /**
   * Rename the transient by creating a new transient with the same
   * value and expiration as the old transient but with a different
   * name and then deleting the old transient.
   *
   * @param string $name Name of the renamed transient
   *
   * @access public
   * @since 0.1
   */
  public function rename( $name ) {
    $new = new transient( $name );
    $new->site( $this->is_site() )
      ->expires( $this->get_expiration() )
      ->set( $this->get_direct() );

    $old = new transient( $this->name );
    $old->site( $this->is_site() )
      ->background( true )
      -sudo( true )
      ->delete();
  }

  /**
   * Get transient
   *
   * @param callable $callback Callback used to potentially generate the value
   * @param array    $args     Array of arguments to send with the callable
   *
   * @access public
   * @since 0.1
   *
   * @return mixed Transient value
   */
  public function get( $callback = '', array $args = [] ) {
    $pre = \apply_filters( 'Boldface\BetterTransients\pre_get_transient', false, $this->name );
    $pre = \apply_filters( "Boldface\BetterTransients\pre_get_transient_{$this->name}", false, $pre );

    if( false !== $pre ) {
      return $this->value = $pre;
    }

    $value = $this->is_background() ? $this->get_background() : $this->get_foreground();

    //* If there isn't a value for the transient try to generate it,
    //* provided the callback is callable
    if( false === $value && is_callable( $callback ) ) {
      $transient = call_user_func_array( $callback, $args );

      $new = new transient( $this->name );
      $new->site( $this->is_site() )
        ->expires( isset( $transient[ 'expires' ] ) ? $transient[ 'expires' ] : 0 )
        ->background( true )
        ->set( isset( $transient[ 'value' ] ) ? $transient[ 'value' ] : 0 );

      $value = $transient[ 'value' ];
    }

    $value = \apply_filters( "Boldface\BetterTransients\getted_transient_{$this->name}", $value, $pre );
    $value = \apply_filters( 'Boldface\BetterTransients\getted_transient', $value, $this->name );

    return $this->value = $value;
  }

  /**
   * Wrapper for get_site_transient
   *
   * @access protected
   * @since 0.1
   */
  protected function get_site_transient() {
    return \get_site_transient( $this->name );
  }

  /**
   * Wrapper for get_transient
   *
   * @access protected
   * @since 0.1
   */
  protected function get_transient() {
    return \get_transient( $this->name );
  }

  /**
   * Determine whether to get the site transient or transient
   *
   * @access protected
   * @since 0.1
   */
  protected function get_foreground() {
    return $this->is_site() ? $this->get_site_transient() : $this->get_transient();
  }

  /**
   * Attempt to get the value directly
   *
   * @access protected
   * @since 0.1
   */
  protected function get_background() {
    $this->is_expired() ? $this->background_process( 'delete' ) : $this->_return();
    return $this->value = $this->get_direct();
  }

  /**
   * Return whether the transient has an expiration
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient has an expiration
   */
  public function has_expiration() {
    return $this->cache->has_expiration();
  }

  /**
   * Return the transient expiration
   *
   * @access public
   * @since 0.1
   *
   * @return mixed The transient expiration
   */
  public function get_expiration() {
    return $this->cache->get_expiration();
  }

  /**
   * Return whether the transient is expired
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is expired
   */
  public function is_expired() {
    return $this->cache->is_expired();
  }

  /**
   * Get the transient value directly
   *
   * @access public
   * @since 0.1
   *
   * @return mixed Transient value
   */
  public function get_direct() {
    return $this->cache->get_direct();
  }

  /**
   * Get transient expiry
   *
   * @access public
   * @since 0.1
   *
   * @return int Transient expiry
   */
  public function expiry() {
    return $this->cache->expiry();
  }

  /**
   * Delete transient
   *
   * @param mixed $value Unused value (optional)
   *
   * @access public
   * @since 0.1
   */
  public function delete( $value = '' ) {
    $pre = \apply_filters( 'Boldface\BetterTransients\pre_delete_transient', false, $this->name );
    $pre = \apply_filters( "Boldface\BetterTransients\pre_delete_transient_{$this->name}", false, $pre );

    if( false !== $pre ) {
      return $this->value = $pre;
    }

    $value = $this->has() && ! $this->can_edit() ? $this->_return() : $this->_delete();

    $value = \apply_filters( "Boldface\BetterTransients\deleted_transient_{$this->name}", $value, $pre );
    $value = \apply_filters( 'Boldface\BetterTransients\deleted_transient', $value, $this->name );

    return $this->value = $value;
  }

  /**
   * Delete transient in foreground or background
   *
   * @access protected
   * @since 0.1
   */
  protected function _delete() {
    return $this->is_background() ? $this->delete_background() : $this->delete_foreground();
  }

  /**
   * Wrapper for delete_site_transient
   *
   * @access protected
   * @since 0.1
   */
  protected function delete_site_transient() {
    \delete_site_transient( 'lock_' . md5( $this->name ) );
    return \delete_site_transient( $this->name );
  }

  /**
   * Wrapper for delete_transient
   *
   * @access protected
   * @since 0.1
   */
  protected function delete_transient() {
    \delete_transient( 'lock_' . md5( $this->name ) );
    return \delete_transient( $this->name );
  }

  /**
   * Determine whether to delete the site transient or transient
   *
   * @access protected
   * @since 0.1
   */
  protected function delete_foreground() {
    return $this->is_site() ? $this->delete_site_transient() : $this->delete_transient();
  }

  /**
   *
   *
   * @access protected
   * @since 0.1
   */
  protected function delete_background() {
    $this->background_process( 'delete' );
    return true;
  }

  /**
   * Set or modify the background property
   *
   * @param bool $background Whether the transient is updated in the background
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function background( $background ) {
    $this->background = $background;
    return $this;
  }

  /**
   * Return whether the transient is updated in the background
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is updated in the background
   */
  protected function is_background() {
    return isset( $this->background ) && false === $this->background ?
      false : true;
  }

  /**
   * Set or Modify the expiration property
   *
   * @param int $expiration The transient expiration
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function expires( $expiration ) {
    $this->expiration = $expiration;
    return $this;
  }

  /**
   * Return whether the transient expires
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient expires
   */
  protected function is_expires() {
    return isset( $this->expiration ) && 0 !== $this->expiration ? true : false;
  }

  /**
   * Set or modify the site property
   *
   * @param bool $site Whether the transient is a site transient
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function site( $site ) {
    $this->site = is_bool( $site ) ? $site : false;
    return $this;
  }

  /**
   * Return whether the transient is a site transient
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the transient is a site transient
   */
  protected function is_site() {
    return isset( $this->site ) ? $this->site : $this->cache->site();
  }

  /**
   * Set or modify the autoload property
   *
   * @param bool $site Whether the transient is autoloaded
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function autoload( $autoload ) {
    $this->autoload = is_bool( $autoload ) ? $autoload : false;
    return $this;
  }

  /**
   * Return whether the transient is autoloaded
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is autoloaded
   */
  protected function is_autoload() {
    return isset( $this->autoload ) ? $this->autoload : $this->cache->autoload();
  }

  /**
   * Set or modify the lock property
   *
   * @param bool $lock Whether the transient is locked
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function lock( $lock ) {
    $this->lock = is_bool( $lock ) ? $lock : true;
    return $this;
  }

  /**
   * Return whether the transient is locked
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the transient is locked
   */
  protected function is_lock() {
    return isset( $this->lock ) ? $this->lock : $this->cache->lock();
  }

  /**
   * Set or modify the sudo property
   *
   * @param bool $sudo Whether the transient is sudo
   *
   * @access public
   * @since 0.1
   *
   * @return object $this instance
   */
  public function sudo( $sudo ) {
    $this->sudo = $this->can_sudo() && is_bool( $sudo ) ? $sudo : false;
    return $this;
  }

  /**
   * Return whether the current user can override the transient lock
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the current user can override the transient lock
   */
  protected function can_sudo() {
    return \current_user_can( 'override_transient_lock' );
  }

  /**
   * Return whether to add, modify, or delete the transient
   *
   * @access protected
   * @since 0.1
   *
   * @return bool whether to add, modify, or delete the transient
   */
  protected function can_edit() {
    return ! $this->is_lock() ? true : $this->is_lock() && $this->can_sudo();
  }

  /**
   * Schedule a background process
   *
   * @access protected
   * @since 0.1
   */
  protected function background_process( $process ) {
    $server = new transient_server();
    $server->schedule( [
      'site'       => $this->site,
      'name'       => $this->name,
      'value'      => $this->value,
      'expiration' => $this->expiration,
      'process'    => $process,
    ] );
  }

  /**
   * Sanitize the input name by stripping all tags, and allowing only
   * alphanumeric characters, plus the dash and underscore.
   *
   * @access protected
   * @since 0.1
   */
  protected function sanitize_name( $name ) {
    $this->name = preg_replace( '/[^A-Za-z0-9-_]/', '', \wp_strip_all_tags( $name ) );
  }

  /**
   * Return
   *
   * @access protected
   * @since 0.1
   */
  protected function _return() {
    return;
  }
}
