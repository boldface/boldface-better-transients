<?php

namespace Boldface\BetterTransients;

defined( 'BOLDFACEBETTERTRANSIENTS' ) or die();

/**
 * Class for interacting with expried WordPress transients
 *
 * @package Boldface\SoftTransients
 */
class transient_server_response {

  /**
   * Add hook, if the _transient is sent via POST
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    if( isset( $_POST[ '_transient' ] ) ) {
      \add_action( 'init', [ $this, 'init' ], -1 );
    }
  }

  /**
   * Fired on init. Looks for the transient nonce and maybe
   * adds a background action
   *
   * @access public
   * @since 0.1
   */
  public function init() {
    if( \wp_verify_nonce( $_POST[ '_wpnonce' ], 'transient-nonce' ) ) {
      $this->background_action( \maybe_unserialize( $_POST[ '_transient' ] ) );
    }
    exit;
  }

  /**
   * Background action to set, get, or delete transient
   *
   * @param array $args Array of arguments
   *
   * @access protected
   * @since 0.1
   */
  protected function background_action( array $args = [] ) {
    if( [] === $args ) {
      exit;
    }

    $process = $args[ 'process' ];

    $transient = new transient( $args[ 'name' ] );
    $transient->background( false )
      ->sudo( true )
      ->site( $this->esc_bool( $args[ 'site' ] ) )
      ->expires( intval( $args[ 'expiration' ] ) )
      ->lock( $this->esc_bool( $args[ 'lock' ] ) )
      ->{$process}( $args[ 'value' ] );
  }

  /**
   * Escape boolean values
   *
   * @param mixed $bool A variable to check whether it's a boolean
   *
   * @access protected
   * @since 0.1
   *
   * @return Whether the value is boolean
   */
  protected function esc_bool( $bool ) {
    return !! $bool;
  }
}
