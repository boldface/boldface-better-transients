<?php

namespace Boldface;

/**
 * Attempt to autoload classes by initiating other classes.
 *
 * @package Boldface
 */
if( ! class_exists( '\Boldface\autoload' ) ) :

class autoload {
  /**
   * Instance of the namespace validator
   *
   * @var namespace_validator
   *
   * @access private
   * @since 0.1
   */
  private $namespace_validator;

  /**
   * Instance of the file registry
   *
   * @var file_registry
   *
   * @access private
   * @since 0.1
   */
  private $file_registry;

  /**
   * Create instance of the class by initiating the namespace validator and
   * file registry.
   *
   * @access private
   * @since 0.1
   */
  public function __construct() {
    require_once( __DIR__ . '/namespace-validator.php' );
    $this->namespace_validator = new namespace_validator();

    require_once( __DIR__ . '/file-registry.php' );
    $this->file_registry = new file_registry();
  }

  /**
   * Load the specified class.
   *
   * @param string $class The class we're attempting to load
   *
   * @access private
   * @since 0.1
   */
  public function load( $class ) {
    if( $this->namespace_validator->is_valid( $class ) ) {
      $this->file_registry->load( $class );
    }
  }
}
endif;
