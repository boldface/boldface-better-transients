# Boldface Better Transients

## Table of Contents
* Synopsis
* Details

** WordPress Core
** Wrapping Core Function
** Extending Core
** Examples

* Requirements
* Installation

## Synopsis
There are only six functions in the WordPress Transients API, yet it may
be the most understood API in WordPress core.

## Details

### WordPress Core
The six functions to interact with transients available in WordPress core are:
* set_transient()
* set_site_transient()
* get_transient()
* get_site_transient()
* delete_transient()
* delete_site_transient()

### Wrapping Core Function

This plugin acts as a wrapper for getting, setting, and deleting transients:
* transient::set()
* transient::get()
* transient::delete()

### Extending Core

Since transients are all stored in the options table, the site aspect
is abstracted from the getting, setting, and deleting of transients,
and replaced with its own method. Further, the option of autoloading or
not autoloading is now available to all transients.

* transient::site()
* transient::autoload()

The class adds several methods to extend the API. Transients are locked by
default, which means that they cannot be modified or deleted except by an
administrator using the sudo method.

* transient::has()
* transient::rename()
* transient::lock()
* transient::sudo()

And a method to access the value directly using the options API instead
of the transients API.

* transient::get_direct()

And adds methods to interact with the expiration

* transient::has_expiration()
* transient::get_expiration()
* transient::is_expired()

### Examples
To set a site transient named Foo with a value of Bar
that expires in an hour and is set in the background:

$example = new \Boldface\Transient\transient( 'Foo' );
$example->site( true )
  ->background( true )
  ->expires( HOUR_IN_SECONDS )
  ->set( 'Bar' );

## Requirements
This plugin has been tested with WordPress 4.7. It requires PHP 5.3 or greater.

## Installation
Download the plugin zip file to your WordPress plugins directory. Extract the
files. Activate the plugin.
